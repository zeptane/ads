package ads

type FilterFn = func(value interface{}) (include bool, continuation bool)
type MapFn = func(value interface{}) (result interface{}, continuation bool)
type ReduceFn = func(acc interface{}, value interface{}) (result interface{}, continuation bool)

type MapReducer interface {
	Map(fn MapFn) interface{}
	Filter(fn FilterFn) interface{}
	Reduce(interface{}, ReduceFn) interface{}
}
