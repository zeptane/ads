package listop

import (
	"ads"
	"sync"
)

var _ ads.MapReducer = &Set{}

type Set struct {
	n           sync.Mutex
	internalMap map[interface{}]bool
}

func NewSet() *Set {
	return &Set{internalMap: map[interface{}]bool{}}
}

func (s *Set) Len() int {
	return len(s.internalMap)
}

func (s *Set) Populate(ks []interface{}) {
	for _, k := range ks {
		s.Set(k)
	}
}

func (s *Set) Duplicate() *Set {
	r := NewSet()
	s.Each(func(x interface{}) bool {
		r.Set(x)
		return true
	})

	return r
}

func (s *Set) Has(k interface{}) bool {
	_, ok := s.internalMap[k]

	return ok
}

func (s *Set) Set(k interface{}) {
	s.n.Lock()
	defer s.n.Unlock()

	s.internalMap[k] = true
}

func (s *Set) Unset(k interface{}) {
	s.n.Lock()
	defer s.n.Unlock()

	delete(s.internalMap, k)
}

func (s *Set) Members() []interface{} {
	rt := make([]interface{}, 0)

	for k, _ := range s.internalMap {
		rt = append(rt, k)
	}

	return rt
}

func (s *Set) All(subset []interface{}) (yes bool) {

	yes = true

	for k, _ := range subset {
		_, here := s.internalMap[k]

		yes = yes && here
	}

	return
}

func (s *Set) Any(subset []interface{}) (yes bool) {

	for k, _ := range subset {
		if _, yes = s.internalMap[k]; yes {
			break
		}
	}
	return
}

func (s *Set) Union(s2 *Set) (r *Set) {

	r = s.Duplicate()

	s2.Each(func(x interface{}) bool {
		r.Set(x)
		return true
	})

	return
}

func (s *Set) Subtraction(s2 *Set) *Set {
	z := s.Filter(func(x interface{}) (bool, bool) {
		return !s2.Has(x), true
	})

	return z.(*Set)

}

func (s *Set) Intersection(s2 *Set) *Set {
	z := s2.Filter(func(x interface{}) (bool, bool) {
		return s.Has(x), true
	})
	return z.(*Set)
}

func (s *Set) Map(fn ads.MapFn) interface{} {
	r := NewSet()

	s.Each(func(i interface{}) bool {
		v, cont := fn(i)
		if !cont {
			return false
		}

		r.Set(v)

		return true
	})

	return r
}

func (s *Set) Filter(fn ads.FilterFn) interface{} {
	r := NewSet()

	s.Each(func(elem interface{}) bool {

		test, cont := fn(elem)
		if !cont {
			return false
		}

		if test {
			r.Set(elem)
		}

		return true
	})

	return r
}

func (s *Set) Reduce(res interface{}, g ads.ReduceFn) interface{} {
	acc := res
	cont := false

	s.Each(func(el interface{}) bool {
		acc, cont = g(acc, el)
		return cont
	})

	return acc
}

func (s *Set) Each(fo func(interface{}) bool) {

	for _, k := range s.Members() {
		if !fo(k) {
			break
		}
	}

}
