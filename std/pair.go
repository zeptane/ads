package std

import (
	"ads/list"
	"fmt"
)

type Pair struct {
	First  interface{}
	Second interface{}
}

func NewPair(f, s interface{}) *Pair {
	return &Pair{
		First:  f,
		Second: s,
	}
}

func (p *Pair) String() string {
	return fmt.Sprintf("(%v, %v)", p.First, p.Second)
}

func (p *Pair) List() list.List {
	return list.NewDblLinkedList(p.First, p.Second)
}

func (p *Pair) Unpack() (a, b interface{}) {
	a = p.First
	b = p.Second
	return
}
