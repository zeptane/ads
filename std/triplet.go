package std

import (
	"ads/list"
	"fmt"
)

type Triplet struct {
	First  interface{}
	Second interface{}
	Third  interface{}
}

func NewTriplet(f, s, t interface{}) *Triplet {
	return &Triplet{
		First:  f,
		Second: s,
		Third:  t,
	}
}

func (p *Triplet) String() string {
	return fmt.Sprintf("(%v, %v, %v)", p.First, p.Second, p.Third)
}

func (p *Triplet) List() list.List {
	return list.NewDblLinkedList(p.First, p.Second, p.Third)
}

func (p *Triplet) Unpack() (a, b, c interface{}) {
	a = p.First
	b = p.Second
	c = p.Third
	return
}
