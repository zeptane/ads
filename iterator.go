package ads

type Iterator interface {
	Each(func(interface{}) bool)
}
