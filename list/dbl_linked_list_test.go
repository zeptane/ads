package list

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const TestElem = "one"

var testSlice = []interface{}{1, 2, 3, 4}

func TestNewDblLinkedList(t *testing.T) {
	y := NewDblLinkedList()
	assert.IsType(t, &DblLinkedList{}, y)
}

func TestNewDblLinkedList2(t *testing.T) {
	y := NewDblLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
	assert.IsType(t, &DblLinkedList{}, y)
	assert.Equal(t, 10, y.Len())
}

func TestDblLinkedList_Populate(t *testing.T) {
	y := &DblLinkedList{}
	y.Populate(testSlice)
	assert.Equal(t, len(testSlice), y.Len())
}

func TestDblLinkedList_Populate2(t *testing.T) {
	y := &DblLinkedList{}
	y.Populate(testSlice)
	y.Populate(testSlice)
	assert.Equal(t, len(testSlice)*2, y.Len())

}

func TestDblLinkedList_Add(t *testing.T) {

	testList := &DblLinkedList{}

	testList.Add(1)
	testList.Add(2)
	testList.Add(3)
	testList.Add(4)

	assert.Equal(t, len(testSlice), testList.Len())
}

func TestDblLinkedList_Empty(t *testing.T) {
	testList := &DblLinkedList{}

	assert.True(t, testList.Empty())

	testList.Add(TestElem)

	assert.False(t, testList.Empty())
}

func TestDblLinkedList_Len(t *testing.T) {
	testList := &DblLinkedList{}

	assert.Equal(t, 0, testList.Len())

	testList.Add(TestElem)

	assert.Equal(t, 1, testList.Len())
}

func TestDblLinkedList_Head(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	assert.NotNil(t, testList.Head())
	assert.Equal(t, testSlice[0], testList.Head())
}

func TestDblLinkedList_Tail(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	tail := testList.Tail()

	assert.IsType(t, &DblLinkedList{}, tail)
	assert.Equal(t, len(testSlice)-1, tail.Len())
}

func TestDblLinkedList_Tail2(t *testing.T) {
	testList := NewDblLinkedList(TestElem)
	assert.Equal(t, 1, testList.Len())

	tail := testList.Tail()
	assert.Nil(t, tail)
}

func TestDblLinkedList_Tail3(t *testing.T) {
	testList := NewDblLinkedList()
	assert.Nil(t, testList.Tail())
}

func TestDblLinkedList_Slice(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	slc := testList.Slice()

	assert.Equal(t, testSlice, slc)
}

func TestDblLinkedList_First(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	assert.Equal(t, testSlice[0], testList.First())
}

func TestDblLinkedList_First2(t *testing.T) {
	testList := NewDblLinkedList()

	assert.Nil(t, testList.First())
}

func TestDblLinkedList_Last(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	assert.Equal(t, testSlice[len(testSlice)-1], testList.Last())
}

func TestDblLinkedList_Last2(t *testing.T) {
	testList := NewDblLinkedList()
	assert.Nil(t, testList.Last())
}
func TestDblLinkedList_InsertFirst(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	testList.InsertFirst(10)
	first := testList.Head()

	assert.Equal(t, len(testSlice)+1, testList.Len())
	assert.Equal(t, 10, first)
}

func TestDblLinkedList_InsertFirst2(t *testing.T) {
	testList := NewDblLinkedList()
	testList.InsertFirst(10)
	first := testList.First()

	assert.Equal(t, 1, testList.Len())
	assert.Equal(t, 10, first)
}

func TestDblLinkedList_Add2(t *testing.T) {

	testList := NewDblLinkedList()

	testList.Add(10)

	assert.Equal(t, 1, testList.Len())
}

func TestDblLinkedList_PopFirst(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	x := testList.PopFirst()

	assert.Equal(t, testSlice[0], x)
	assert.Equal(t, len(testSlice)-1, testList.Len())
}

func TestDblLinkedList_PopFirst2(t *testing.T) {
	testList := NewDblLinkedList()

	x := testList.PopFirst()

	assert.Nil(t, x)
}

func TestDblLinkedList_PopLast(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	x := testList.PopLast()

	assert.Equal(t, testSlice[len(testSlice)-1], x)
	assert.Equal(t, len(testSlice)-1, testList.Len())
}

func TestDblLinkedList_PopLast2(t *testing.T) {
	testList := NewDblLinkedList()

	x := testList.PopLast()

	assert.Nil(t, x)
}

func TestDblLinkedList_Nth(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	x := testList.Nth(2)

	assert.Equal(t, testSlice[2], x)
}

func TestDblLinkedList_Nth2(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	assert.Panics(t, func() {
		testList.Nth(1024)
	})
}

func TestDblLinkedList_Nth3(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	assert.Panics(t, func() {
		testList.Nth(-1)
	})
}

func TestDblLinkedList_Nth4(t *testing.T) {
	testList := NewDblLinkedList()

	assert.Panics(t, func() {
		testList.Nth(0)
	})
}

func TestDblLinkedList_Nth5(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	x := testList.Nth(0)

	assert.Equal(t, testSlice[0], x)
}

func TestDblLinkedList_Nth6(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	x := testList.Nth(testList.Len() - 1)

	assert.Equal(t, testSlice[len(testSlice)-1], x)
}

func TestDblLinkedList_Each(t *testing.T) {
	testList := NewDblLinkedList()

	for x := 0; x < 1000; x++ {
		testList.Add(x)
	}

	m := 0

	testList.Each(func(v interface{}) bool {
		m++
		return true
	})

	assert.Equal(t, 1000, m)
}

func TestDblLinkedList_Each2(t *testing.T) {
	testList := NewDblLinkedList()

	for x := 0; x < 1000; x++ {
		testList.Add(x)
	}

	m := 0

	// the same, but Each stopped in half-way
	testList.Each(func(v interface{}) bool {

		if m >= 500 {
			return false
		}
		m++
		return true
	})

	assert.Equal(t, 500, m)
}

func TestDblLinkedList_Filter(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	filtered := testList.Filter(func(v interface{}) (cont, ok bool) {
		i, _ := v.(int)

		return i == 3, true // filter out all elements which are int(3)
	})

	assert.IsType(t, &DblLinkedList{}, filtered)
	asList := filtered.(*DblLinkedList)

	assert.Equal(t, 1, asList.Len())
	assert.Equal(t, 3, asList.First())

}

func TestDblLinkedList_Map(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	newList := testList.Map(func(v interface{}) (ret interface{}, cont bool) {
		ret = v.(int) * 10
		cont = true
		return
	})

	assert.IsType(t, &DblLinkedList{}, newList)

	asList := newList.(*DblLinkedList)
	// every new element must be multiplication of ten

	for i, x := range asList.Slice() {
		assert.Equal(t, testSlice[i], x.(int)/10)

	}
}

func TestDblLinkedList_Reduce(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	rv := testList.Reduce(0, func(i, v interface{}) (r interface{}, cont bool) {

		r = i.(int) + v.(int)
		cont = true

		return
	})

	assert.IsType(t, 0, rv)
	assert.Equal(t, 10, rv)
}

func TestDblLinkedList_Remove(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.Remove(3)

	assert.Equal(t, len(testSlice)-1, testList.Len())

	found := false

	for _, x := range testList.Slice() {
		if x == 3 {
			found = true
			break
		}
	}

	assert.False(t, found)
}

func TestDblLinkedList_InsertAt(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)
	testList.InsertAt(2, 7)

	assert.Equal(t, len(testSlice)+1, testList.Len())
	assert.Equal(t, 7, testList.Nth(2))
}

func TestDblLinkedList_InsertAt2(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	assert.Panics(t, func() {
		testList.InsertAt(1000, 7)
	})
}

func TestDblLinkedList_InsertAt3(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	assert.Panics(t, func() {
		testList.InsertAt(-1, 7)
	})
}

func TestDblLinkedList_InsertAt4(t *testing.T) {
	testList := NewDblLinkedList()

	testList.InsertAt(0, 7)

	assert.Equal(t, 1, testList.Len())
	assert.Equal(t, 7, testList.First())

}

func TestDblLinkedList_InsertAt5(t *testing.T) {
	testList := NewDblLinkedList()

	assert.Panics(t, func() {
		testList.InsertAt(90000, 7)
	})
}

func TestDblLinkedList_RemoveAt(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.RemoveAt(1)

	assert.Equal(t, len(testSlice)-1, testList.Len())
	assert.Equal(t, 3, testList.Nth(1))
}

func TestDblLinkedList_RemoveAt2(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.RemoveAt(0)

	assert.Equal(t, len(testSlice)-1, testList.Len())
	assert.Equal(t, 2, testList.First())
}

func TestDblLinkedList_RemoveAt3(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.RemoveAt(0)

	assert.Equal(t, len(testSlice)-1, testList.Len())
	assert.Equal(t, 2, testList.First())
}

func TestDblLinkedList_RemoveAt4(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.RemoveAt(testList.Len() - 1)

	assert.Equal(t, len(testSlice)-1, testList.Len())
	assert.Equal(t, 3, testList.Last())
}

func TestDblLinkedList_RemoveAt5(t *testing.T) {
	testList := NewDblLinkedList()

	assert.Panics(t, func() {
		testList.RemoveAt(0)
	})

}

func TestDblLinkedList_Clear(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	testList.Clear()

	assert.Equal(t, 0, testList.Len())
	assert.Len(t, testList.Slice(), 0)

}

func TestDblLinkedList_Clear2(t *testing.T) {
	testList := NewDblLinkedList()

	testList.Clear()

	assert.Equal(t, 0, testList.Len())
	assert.Len(t, testList.Slice(), 0)
}

func TestDblLinkedList_Find(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	r := testList.Find(3)

	assert.NotNil(t, r)
	assert.Equal(t, 3, r)
}

func TestDblLinkedList_Find2(t *testing.T) {
	testList := NewDblLinkedList(testSlice...)

	r := testList.Find("somewhat")

	assert.Nil(t, r)

}
