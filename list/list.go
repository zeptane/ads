package list

type ListElement struct {
	next  *ListElement
	prev  *ListElement
	Value interface{}
}

func (l *ListElement) Cleanup() {
	l.next = nil
	l.prev = nil
	l.Value = nil
}

type List interface {
	First() interface{}
	Last() interface{}
	Nth(int) interface{}
	InsertFirst(interface{})
	InsertAt(int, interface{})
	Add(interface{})
	PopFirst() interface{}
	PopLast() interface{}
	Remove(interface{})
	RemoveAt(int)
	Head() interface{}
	Tail() List
	Len() int
	Empty() bool
	Slice() []interface{}
	Clear()
	Find(interface{}) interface{}
}
