package list

import "github.com/zeptane/ads"

var _ List = &DblLinkedList{}
var _ ads.Iterator = &DblLinkedList{}
var _ ads.MapReducer = &DblLinkedList{}

type DblLinkedList struct {
	first    *ListElement
	last     *ListElement
	capacity int
}

func NewDblLinkedList(elems ...interface{}) *DblLinkedList {
	n := &DblLinkedList{}

	if len(elems) > 0 {
		n.Populate(elems)
	}
	return n
}

func (d *DblLinkedList) Populate(x []interface{}) {
	for _, y := range x {
		d.Add(y)
	}
}

func (d *DblLinkedList) Len() int {
	return d.capacity
}

func (d *DblLinkedList) Empty() bool {
	return d.first == nil && d.last == nil
}

func (d *DblLinkedList) First() interface{} {
	if !d.Empty() {
		return d.first.Value
	} else {
		return nil
	}
}

func (d *DblLinkedList) Last() interface{} {
	if !d.Empty() {
		return d.last.Value
	} else {
		return nil
	}
}

func (d *DblLinkedList) Nth(n int) interface{} {
	return d.skipToElementByNumber(n).Value
}

func (d *DblLinkedList) InsertFirst(v interface{}) {
	newElem := &ListElement{Value: v}
	if d.Empty() {
		d.first = newElem
		d.last = newElem
	} else {
		newElem.next = d.first
		d.first.prev = newElem
		d.first = newElem
	}
	d.capacity += 1
}

func (d *DblLinkedList) Add(v interface{}) {
	if d.Empty() {
		d.InsertFirst(v)
	} else {
		newElem := &ListElement{Value: v, prev: d.last}
		d.last.next = newElem
		d.last = newElem
		d.capacity += 1
	}

}

func (d *DblLinkedList) Remove(v interface{}) {

	if d.Empty() {
		panic("trying to remove value from empty list")
	}

	x := d.first

	for x != nil {
		if x.Value == v {
			prevNode := x.prev
			nextNode := x.next
			prevNode.next = nextNode
			x.Cleanup()
			d.capacity -= 1
			break
		}

		x = x.next
	}

}

func (d *DblLinkedList) InsertAt(n int, v interface{}) {
	if d.Empty() && n != 0 {
		panic("cannot insert element into an empty list at position, other that 0")
	}

	if d.Empty() || n == 0 {
		d.InsertFirst(v)
	} else if n == (d.capacity - 1) {
		d.Add(v)
	} else {
		node := d.skipToElementByNumber(n)
		prevNode := node.prev
		newNode := &ListElement{next: node, prev: prevNode, Value: v}
		prevNode.next = newNode
		node.prev = newNode

		d.capacity += 1
	}
}

func (d *DblLinkedList) RemoveAt(n int) {

	if d.Empty() {
		panic("can't remove anything from empty list")
	}

	if n == 0 {
		d.PopFirst()
	} else if n == (d.capacity - 1) {
		d.PopLast()
	} else {
		node := d.skipToElementByNumber(n)

		prevNode := node.prev
		nextNode := node.next
		prevNode.next = nextNode

		node.Cleanup()
		d.capacity -= 1
	}
}

func (d *DblLinkedList) PopFirst() interface{} {
	if d.Empty() {
		return nil
	}
	v := d.first.Value

	newHead := d.first.next

	d.first.Cleanup()
	d.first = newHead

	d.capacity -= 1

	return v
}

func (d *DblLinkedList) PopLast() interface{} {
	if d.Empty() {
		return nil
	}
	v := d.last.Value

	newLastNode := d.last.prev

	d.last.Cleanup()
	d.last = newLastNode

	d.capacity -= 1

	return v
}

func (d *DblLinkedList) Head() interface{} {
	if d.Empty() {
		return nil
	} else {
		return d.first.Value
	}
}

func (d *DblLinkedList) Tail() List {
	if d.Empty() {
		return nil
	} else {
		nextNode := d.first.next
		if nextNode == nil {
			return nil
		}

		newHead := &ListElement{Value: nextNode.Value, next: nextNode.next}
		newList := &DblLinkedList{
			first:    newHead,
			last:     d.last,
			capacity: d.capacity - 1,
		}

		return newList
	}
}

func (d *DblLinkedList) Map(f func(interface{}) (interface{}, bool)) interface{} {
	l := &DblLinkedList{}

	d.Each(func(v interface{}) bool {

		newValue, cont := f(v)

		if !cont {
			return false
		}

		l.Add(newValue)

		return true
	})

	return l
}

func (d *DblLinkedList) Filter(f func(interface{}) (bool, bool)) interface{} {
	l := &DblLinkedList{}

	d.Each(func(v interface{}) bool {

		okay, cont := f(v)

		if !cont {
			return false
		}

		if okay {
			l.Add(v)
		}

		return true
	})

	return l
}

func (d *DblLinkedList) Reduce(initial interface{}, r func(interface{}, interface{}) (interface{}, bool)) interface{} {
	cont := true
	rv := initial

	d.Each(func(v interface{}) bool {

		rv, cont = r(rv, v)
		return cont
	})

	return rv
}

func (d *DblLinkedList) Each(g func(interface{}) bool) {

	for node := d.first; node != nil; node = node.next {
		if !g(node.Value) {
			break
		}
	}
}

func (d *DblLinkedList) Slice() []interface{} {
	r := []interface{}{}

	d.Each(func(v interface{}) bool {
		r = append(r, v)
		return true
	})
	return r
}

func (d *DblLinkedList) skipToElementByNumber(n int) *ListElement {
	if n >= d.capacity || n < 0 {
		panic("list index out of bounds")
	}

	if n == 0 {
		return d.first
	}

	if n == (d.capacity - 1) {
		return d.last
	}

	node := d.first

	for cnt := 0; cnt <= n; cnt++ {
		node = node.next
		cnt++
	}
	return node
}

func (d *DblLinkedList) Clear() {

	for node := d.first; node != nil; {
		next := node.next

		node.next = nil
		node.prev = nil
		node.Value = nil

		node = next
	}

	d.first = nil
	d.last = nil

	d.capacity = 0
}

func (d *DblLinkedList) Find(v interface{}) (r interface{}) {

	r = d.Reduce(r, func(rv, v1 interface{}) (interface{}, bool) {
		if v1 == v {
			return v1, false
		} else {
			return nil, true
		}

	})

	return
}
