package _map

import (
	"github.com/zeptane/ads"
	list "github.com/zeptane/ads/list"
	"github.com/zeptane/ads/std"
)

var _ Map = &OrderedMap{}
var _ ads.MapReducer = &OrderedMap{}

// Structure, which preserves put ordering
// in other words it is a sort of hash array
type OrderedMap struct {
	backedMap  map[interface{}]interface{}
	backedList *list.DblLinkedList
}

func NewOrderedMap() *OrderedMap {
	return &OrderedMap{
		backedMap:  map[interface{}]interface{}{},
		backedList: list.NewDblLinkedList(),
	}
}

func (o *OrderedMap) Len() int {
	return len(o.backedMap)
}

func (o *OrderedMap) Clear() {
	o.backedMap = map[interface{}]interface{}{}
	o.backedList.Clear()
}

func (o *OrderedMap) Duplicate() *OrderedMap {
	newMap := NewOrderedMap()

	for _, p := range o.KeyValues() {
		newMap.SetPair(p)
	}

	return newMap
}

func (o *OrderedMap) Get(key interface{}) (r interface{}, k bool) {
	r, k = o.backedMap[key]
	return
}

func (o *OrderedMap) Set(k interface{}, v interface{}) {
	if _, already := o.backedMap[k]; !already {
		o.backedList.Add(k)
	}

	o.backedMap[k] = v

}

func (o *OrderedMap) Has(k interface{}) bool {
	_, has := o.backedMap[k]
	return has
}

func (o *OrderedMap) Del(k interface{}) {
	if o.Has(k) {
		delete(o.backedMap, k)
		o.backedList.Remove(k)
	}
}

func (o *OrderedMap) GetPair(k interface{}) (*std.Pair, bool) {
	if r, k := o.Get(k); k {
		return std.NewPair(k, r), true
	} else {
		return nil, false
	}
}

func (o *OrderedMap) SetPair(pair *std.Pair) {
	o.Set(pair.First, pair.Second)
}

func (o *OrderedMap) Keys() []interface{} {
	return o.backedList.Slice()
}

// return values in order of their appearance
func (o *OrderedMap) Values() []interface{} {
	vs := make([]interface{}, 0)

	o.backedList.Each(func(k interface{}) bool {

		v, _ := o.Get(k)
		vs = append(vs, v)

		return true
	})

	return vs
}

func (o *OrderedMap) KeyValues() []*std.Pair {
	vs := make([]*std.Pair, 0)

	o.backedList.Each(func(k interface{}) bool {

		v, _ := o.Get(k)
		vs = append(vs, std.NewPair(k, v))

		return true
	})

	return vs
}

func (o *OrderedMap) Nth(n int) *std.Pair {
	k := o.backedList.Nth(n)
	v, _ := o.GetPair(k)
	return v
}

func (o *OrderedMap) First() *std.Pair {
	k := o.backedList.First()
	v, _ := o.GetPair(k)
	return v

}

func (o *OrderedMap) Last() *std.Pair {
	k := o.backedList.Last()
	v, _ := o.GetPair(k)
	return v

}

func (o *OrderedMap) Each(g func(*std.Pair) bool) {
	o.backedList.Each(func(k interface{}) bool {
		v, _ := o.GetPair(k)
		return g(v)
	})
}

func (o *OrderedMap) Map(g ads.MapFn) interface{} {
	m := NewOrderedMap()

	o.Each(func(p *std.Pair) bool {
		res, cont := g(p)
		if !cont {
			return false
		}
		// let's panic here, we are not accepting anything, but Pair here
		// TODO: must be specified in docs
		toP := res.(*std.Pair)
		m.SetPair(toP)

		return true
	})

	return m
}

func (o *OrderedMap) Filter(g ads.FilterFn) interface{} {
	m := NewOrderedMap()

	o.Each(func(p *std.Pair) bool {
		res, cont := g(p)
		if !cont {
			return false
		}

		if res {
			m.SetPair(p)
		}
		return true
	})
	return m
}

func (o *OrderedMap) Reduce(initial interface{}, g ads.ReduceFn) (rv interface{}) {
	cont := false
	acc := initial

	o.Each(func(p *std.Pair) bool {
		acc, cont = g(acc, p)
		return cont
	})

	return acc
}
