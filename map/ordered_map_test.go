package _map

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewOrderedMap(t *testing.T) {
	tt := NewOrderedMap()
	assert.IsType(t, &OrderedMap{}, tt)
}

func TestOrderedMap_GetSetKeyValues(t *testing.T) {
	var v interface{}
	var b bool

	tt := NewOrderedMap()

	tt.Set("one", 1)
	tt.Set("two", 2)
	tt.Set("three", 3)

	assert.Len(t, tt.KeyValues(), 3)

	v, b = tt.Get("one")
	assert.True(t, b)
	assert.Equal(t, 1, v)

	v, b = tt.Get("two")
	assert.True(t, b)
	assert.Equal(t, 2, v)

	v, b = tt.Get("three")
	assert.True(t, b)
	assert.Equal(t, 3, v)

}
