package _map

import (
	"github.com/zeptane/ads/std"
)

// Basic type for every map-like object
type Map interface {
	Get(interface{}) (interface{}, bool)
	Set(interface{}, interface{})

	GetPair(interface{}) (*std.Pair, bool)
	SetPair(pair *std.Pair)

	Has(interface{}) bool
	Del(interface{})
	Keys() []interface{}
	Values() []interface{}
	KeyValues() []*std.Pair
	Clear()
	Len() int
}
